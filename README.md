# Self-Hosted Gitlab Runners in K8s

Demo repo: https://gitlab.com/vleermuis/gitlab-runners

## Prereqs

We need a kind cluster on a clean vm.

1. Create a host vm using [public-infra](https://github.com/cloudymax/public-infra/tree/main/virtual-machines/qemu)

2. Provision

	```zsh
	# install docker/kubectl deps
	sudo apt-get -y install \
		apt-transport-https \
		ca-certificates \
		curl \
		gnupg-agent \
		software-properties-common

	# download the gpg keys we need 
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | 	sudo apt-key add -
	sudo curl -fsSLo /usr/share/keyrings/	kubernetes-archive-keyring.gpg https://packages.cloud.google.	com/apt/doc/apt-key.gpg
	curl https://baltocdn.com/helm/signing.asc | sudo apt-key 	add -

	# Add the repositories 
	# Docker requires the appropriate ubuntu flavor for docker, in this case "focal".
	sudo add-apt-repository "deb [arch=amd64] https://download.	docker.com/linux/ubuntu focal stable"

	echo "deb [signed-by=/usr/share/keyrings/	kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

	echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list

	# update the package cache 
	sudo apt-get -y update

	# actually install something :D
	sudo apt-get -y install \
		docker-ce \
		docker-ce-cli \
		containerd.io \
		kubectl \
		helm

	# create a group for docker, and add the user to it for 	sudoless docker
	sudo apt-get -y update
	groupadd docker
	sudo usermod -aG docker $USER
	newgrp docker

	# install KIND
	curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.13.0/	kind-linux-amd64
	chmod +x ./kind
	sudo mv ./kind /usr/bin/kind

	# Create the KIND cluster
	kind create cluster --name="smolk8s"
	kubectl cluster-info --context kind-smolk8s
	kubectl create namespace gitlab-runner
	
	```

## Registering your Runners
https://docs.gitlab.com/runner/install/kubernetes.html

1. Clone the Helm chart repo to get a copy of the values.yaml file.

	```bash
	git clone https://gitlab.com/gitlab-org/charts/gitlab-runner
	```

2. Go to your group or project settings and under `CI/CD` select `Runners`

3. Select 'Register a New Runner' at the top right of the screen

4. Uncomment and add the token to the values.yaml file as `runnerRegistrationToken`

4. Uncomment and change `gitlabUrl` to "https://gitlab.com/"


## Installing Gitlab-Runner with Helm

https://docs.gitlab.com/runner/install/kubernetes.html

1. Clone the chart repo to get a copy of the values.yaml file.

	```bash
	git clone https://gitlab.com/gitlab-org/charts/gitlab-runner
	```

2. add the base gitlab helm chart repository

	```bash
	helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
	helm repo add gitlab https://charts.gitlab.io
	helm repo update
	```


```zsh
helm install --namespace gitlab-runner gitlab-runner -f values.yaml gitlab/gitlab-runner

```